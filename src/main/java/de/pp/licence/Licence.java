package de.pp.licence;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Licence {
	private final LicenceDetails details;
	private final String key;
}