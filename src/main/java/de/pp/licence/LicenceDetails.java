package de.pp.licence;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class LicenceDetails {
	private final String customer;
	private final String validuntil;
	private final Integer configurations;
}