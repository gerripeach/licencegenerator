package de.pp.licence;

import com.beust.jcommander.Parameter;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public final class CliConfig {

	@Parameter(names = { "-p",
			"--private-key-location" }, description = "Specify the location of the private key. If not set a new key pair will be generated")
	private String privateKeyLocation;

	@Parameter(names = "-c", required = true)
	private String customerName;

	@Parameter(names = "-v")
	private String validUntil;

	@Parameter(names = "-a")
	private Integer configurations = 1;

	@Parameter(names = { "-h", "--help" }, description = "Prints this help message", help = true)
	private Boolean help = false;
}
