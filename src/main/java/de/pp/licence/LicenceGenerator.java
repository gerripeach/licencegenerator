package de.pp.licence;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

import com.beust.jcommander.JCommander;
import com.google.common.base.Strings;
import com.google.gson.Gson;

public class LicenceGenerator {

	private static final Gson gson = new Gson();

	public static void main(String[] args) throws Exception {
		CliConfig config = new CliConfig();
		JCommander jCommander = new JCommander(config);
		jCommander.parse(args);
		if (config.getHelp()) {
			jCommander.usage();
			return;
		}

		String dateString = config.getValidUntil();
		if (!Strings.isNullOrEmpty(dateString)) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate localDate = LocalDate.parse(dateString, formatter);

			if (localDate == null) {
				System.err.println("Wrong date format");
				return;
			}
		}

		new LicenceGenerator().generate(config);
	}

	public void generate(CliConfig config) throws Exception {
		LicenceDetails details = generateLicenceDetails(config);

		PrivateKey privateKey = getPrivateKey(config.getPrivateKeyLocation());

		if (privateKey == null) {
			KeyPair keyPair = generateKeyPair();
			privateKey = keyPair.getPrivate();
			Files.write(Paths.get("private"), keyPair.getPrivate().getEncoded());
			Files.write(Paths.get("public"), keyPair.getPublic().getEncoded());
			String publicKeyReadable = Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
			Files.write(Paths.get("public.readable"), publicKeyReadable.getBytes());

		}

		String detailString = gson.toJson(details);
		String signature = sign(detailString, privateKey);

		PublicKey publicKey = getPublicKey("public");
		validateSignature(publicKey, signature, detailString);

		System.out.println(signature);

		Licence licence = new Licence(details, signature);
		String licenceString = gson.toJson(licence);

		Files.write(Paths.get("licence.json"), licenceString.getBytes());

		System.out.println("ok");
	}

	private LicenceDetails generateLicenceDetails(CliConfig config) {
		return new LicenceDetails(config.getCustomerName(), config.getValidUntil(), config.getConfigurations());
	}

	public static KeyPair generateKeyPair() throws Exception {
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
		generator.initialize(2048, new SecureRandom());
		return generator.generateKeyPair();
	}

	public static String sign(String plainText, PrivateKey privateKey) throws Exception {
		Signature privateSignature = Signature.getInstance("SHA256withRSA");
		privateSignature.initSign(privateKey);
		privateSignature.update(plainText.getBytes(StandardCharsets.UTF_8));

		byte[] signature = privateSignature.sign();

		return Base64.getEncoder().encodeToString(signature);
	}

	public static boolean verify(String plainText, String signature, PublicKey publicKey) throws Exception {
		Signature publicSignature = Signature.getInstance("SHA256withRSA");
		publicSignature.initVerify(publicKey);
		publicSignature.update(plainText.getBytes(StandardCharsets.UTF_8));

		byte[] signatureBytes = Base64.getDecoder().decode(signature);

		return publicSignature.verify(signatureBytes);
	}

	public static PrivateKey getPrivateKey(String filename) throws Exception {

		Path path = Paths.get(filename);
		if (path.toFile() == null || !path.toFile().exists()) {
			return null;
		}
		byte[] keyBytes = Files.readAllBytes(Paths.get(filename));

		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(spec);
	}

	public static PublicKey getPublicKey(String filename) throws Exception {

		byte[] keyBytes = Files.readAllBytes(Paths.get(filename));

		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}

	private boolean validateSignature(PublicKey publicKey, String signature, String content) throws Exception {
		// Let's check the signature
		boolean isCorrect = verify(content, signature, publicKey);
		System.out.println("Signature correct: " + isCorrect);
		return isCorrect;
	}
}
